# About
This plugin will let you spend xp into a skill/attribute/vital via commands. It will spend max up to 2bil xp per command, and will stop when the skill is maxed or you are out of xp.

# Usage  

  /xph skill <skill_id> <raise_amount>    ie:  /xph skill 6 10 (raise melee d 10 points)  
  /xph attr <attr_id> <raise_amount>    ie:  /xph attr 3 23 (raise quickness 23 points)  
  /xph vital <vital_id> <raise_amount>    ie:  /xph vital 1 100 (raise health by 100 points)  

# skills:
        CurrentMeleeDefense = 6,
        CurrentMissileDefense = 7,
        CurrentArcaneLore = 14,
        CurrentMagicDefense = 15,
        CurrentManaConversion = 16,
        CurrentItemTinkering = 18,
        CurrentAssessPerson = 19,
        CurrentDeception = 20,
        CurrentHealing = 21,
        CurrentJump = 22,
        CurrentLockpick = 23,
        CurrentRun = 24,
        CurrentAssessCreature = 27,
        CurrentWeaponTinkering = 28,
        CurrentArmorTinkering = 29,
        CurrentMagicItemTinkering = 30,
        CurrentCreatureEnchantment = 31,
        CurrentItemEnchantment = 32,
        CurrentLifeMagic = 33,
        CurrentWarMagic = 34,
        CurrentLeadership = 35,
        CurrentLoyalty = 36,
        CurrentFletchingSkill = 37,
        CurrentAlchemySkill = 38,
        CurrentCookingSkill = 39,
        CurrentSkillSalvaging = 40,
        CurrentTwoHandedCombat = 41,
        CurrentGearcraft = 42,
        CurrentVoidMagic = 43,
        CurrentHeavyWeapons = 44,
        CurrentLightWeapons = 45,
        CurrentFinesseWeapons = 46,
        CurrentMissileWeapons = 47,
        CurrentShield = 48,
        CurrentDualWield = 49,
        CurrentRecklessness = 50,
        CurrentSneakAttack = 51,
        CurrentDirtyFighting = 52,

# attributes:
        CurrentStrength = 1,
        CurrentEndurance = 2,
        CurrentQuickness = 3,
        CurrentCoordination = 4,
        CurrentFocus = 5,
        CurrentSelf = 6,

# vitals:
        MaximumHealth = 1,
        MaximumStamina = 3,
        MaximumMana = 5,
`